/**
 * Copyright (C) 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package conf;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import aplicacao.nucleo.AplAfastamento;
import aplicacao.nucleo.AplAfastamentoImp;
// tem que explicitar cada classe e interface
import dao.nucleo.AfastamentoDAO;
import dao.nucleo.JPAafastamentoDAO;
import dao.secretaria.PessoaDAO;
import dao.secretaria.JPApessoaDAO;
import aplicacao.secretaria.AplPessoa;
import aplicacao.secretaria.AplPessoaImp;


@Singleton
public class Module extends AbstractModule {
    
	@Override
    protected void configure() {
        
        bind(StartupActions.class);  
        
        bind(AplAfastamento.class).to(AplAfastamentoImp.class);
        bind(AfastamentoDAO.class).to(JPAafastamentoDAO.class);
        bind(AfastamentoDAO.class).to(JPAafastamentoDAO.class);
        bind(PessoaDAO.class).to(JPApessoaDAO.class);
        bind(AplPessoa.class).to(AplPessoaImp.class);
    }

}
