package dao.secretaria;

import models.nucleo.Parentesco;
import dao.nucleo.BaseDAO;

public interface ParentescoDAO extends BaseDAO<Parentesco>{
	
	public Parentesco buscaId(String id_parentesco);
	
	public Boolean checaParentesco(String id_pessoa1, String id_pessoa2);
}
