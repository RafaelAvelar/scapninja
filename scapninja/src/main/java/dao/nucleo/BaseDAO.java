package dao.nucleo;

public interface BaseDAO<T> {
	public void salvar (T object);
	
	T merge(T object);
}
