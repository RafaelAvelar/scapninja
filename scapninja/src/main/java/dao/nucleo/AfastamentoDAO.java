package dao.nucleo;

import java.util.List;

import models.nucleo.Afastamento;
import dao.nucleo.BaseDAO;

public interface AfastamentoDAO extends BaseDAO<Afastamento> {

	public Afastamento buscaId(String id_afastamento);
	
	public List<Afastamento> listaAfastamentos();

}
