package aplicacao.secretaria;

import java.util.List;

import com.google.inject.Inject;

import models.nucleo.Documento;
import dao.secretaria.DocumentoDAO;

public class AplDocumentoImp implements AplDocumento{

	@Inject
	private DocumentoDAO documentoDAO;
	
	@Override
	public void salvar(Documento documento) {
		documentoDAO.salvar(documento);
		
	}

	@Override
	public Documento buscaId(String id_documento) {
		Documento documento = documentoDAO.buscaId(id_documento);
		return documento;
	}

	@Override
	public List<Documento> buscaPorAfastamento(String id_afastamento) {
		return documentoDAO.buscaPorAfastamento(id_afastamento);
	}

}
