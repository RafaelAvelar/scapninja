package aplicacao.secretaria;

import java.util.List;

import models.nucleo.Documento;

public interface AplDocumento {
	
	public void salvar(Documento documento);
	
	public Documento buscaId(String id_documento);
	
	public List<Documento> buscaPorAfastamento(String id_afastamento);
	
}
