package aplicacao.secretaria;
import com.google.inject.Inject;

import models.nucleo.Mandato;
import models.nucleo.Pessoa;
import dao.secretaria.MandatoDAO;
import dao.secretaria.PessoaDAO;

public class AplMandatoImp implements AplMandato{

	@Inject
	private PessoaDAO pessoaDAO;
	
	@Inject 
	private MandatoDAO mandatoDAO;
	
	@Override
	public void salvar(Mandato novoMandato, String matricula) {
		Pessoa chefeDepatamento;
		chefeDepatamento = pessoaDAO.buscaMatricula(matricula);
		novoMandato.setPessoa(chefeDepatamento);
		mandatoDAO.salvar(novoMandato);
	}

	@Override
	public boolean checaMandato(String id_pessoa) {
		return mandatoDAO.checaMandato(id_pessoa);
	}

}
