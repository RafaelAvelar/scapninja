package aplicacao.secretaria;

import java.util.List;

import models.nucleo.Pessoa;

public interface AplPessoa {
	void salvar(Pessoa novoUsuario);
	
	Pessoa buscaMatricula(String matricula);
	
	List<Pessoa> buscaNome(String nome,String sobreNome);
}
