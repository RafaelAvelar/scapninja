package controllers;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;

import com.google.inject.servlet.SessionScoped;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.session.Session;
import ninja.websockets.WebSocketHandshake;


@SessionScoped
public class ScapWebSocket extends Endpoint implements MessageHandler.Whole {

	
	public Result handshake(Context context, WebSocketHandshake handshake) {
        // negotiate the protocol (not always used by clients)
        
		
		
		handshake.selectProtocol("scap");
        
        System.out.println(context.getSession().getId());
        
        return Results.webSocketContinue();
	
	}
	

	@Override
	public void onMessage(Object message) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onOpen(javax.websocket.Session session, EndpointConfig config) {
		// TODO Auto-generated method stub
		System.out.println("CLIENT onOpen: " + session.getId());
		session.addMessageHandler(new MessageHandler.Whole<String>() {
			
			@Override
			public void onMessage(String message){
				System.out.println("### CLIENT: Message received: " + message);

			}
		});
	}
}