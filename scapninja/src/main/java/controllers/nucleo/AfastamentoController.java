package controllers.nucleo;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.persistence.NoResultException;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.lang.String;

import com.google.common.collect.Maps;


import models.nucleo.Afastamento;
import models.nucleo.SituacaoSolic;
import models.nucleo.TipoAfastamento;
import models.nucleo.Onus;
import aplicacao.nucleo.AplAfastamento;
import aplicacao.secretaria.AplPessoa;



import ninja.Result;
import ninja.Results;
import ninja.jpa.UnitOfWork;
import ninja.params.Param;
import ninja.session.Session;
import ninja.Context;


import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.google.inject.servlet.SessionScoped;

import models.nucleo.Onus; 
import models.nucleo.Pessoa;


@Singleton

public class AfastamentoController {
	
	
	
	
	@Inject // será que ele n ta pegando o mesmo objeto dentre chamadas do método?
	Afastamento Afastamento_temp;
	
	
	@Inject
	AplPessoa aplPessoa;
	
	Calendar calendar;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Calendar inicio = new GregorianCalendar();
	Calendar fim = new GregorianCalendar();
	
	@Inject
	Pessoa solicitante;
	@Inject
	AplAfastamento inserir_afastamento;
	
	
	
	String matriculaSolicitante;
	


public Result cadastroAfastamento(Context context) {
	
	List<String> onus = new ArrayList<>();
	onus.add(Onus.INEXISTENTE.toString());
	onus.add(Onus.PARCIAL.toString());
	onus.add(Onus.TOTAL.toString());
	
	
	Result result = Results.html();
	result.render("onus",onus);
	
	context.getSession().clear();//limpando a sessao antes do afastamento, apagar dps
	System.out.println("limpando a sessao, apagar depois");
	return result;
    }

@UnitOfWork
public Result processaFormP1(
        @Param("matricula") String matricula
       ,@Param("onus") Onus onus
        ,@Param("inicioAfastamento") Date inicioAfastamento
        ,@Param("fimAfastamento") Date fimAfastamento
        ,@Param("motivo") String motivo
        ,Context context
        
        //,Afastamento Afastamento_temp
        ){
	
	//testa se o usuário existe.
	matriculaSolicitante=matricula;
	try{
	solicitante = aplPessoa.buscaMatricula(matricula);
	}catch(NoResultException e1) {
		System.out.println("usuario não existe");
		return Results.redirect("/afastamento");
	}
	
	
	
	context.getSession().put("matricula", matricula);
	System.out.print("Estamos no primeiro form. Pegou os dados da sessão: ");
	System.out.println(context.getSession().get("matricula"));	
	
	inicio.setTime(inicioAfastamento);
	fim.setTime(fimAfastamento);
	
	dateFormat.setCalendar(inicio);
	dateFormat.setCalendar(fim);
	
	Afastamento_temp.setData_iniAfast(inicio);
	Afastamento_temp.setData_fimAfast(fim);
	 
	Afastamento_temp.setOnus(onus);
	Afastamento_temp.setMotivo_afast(motivo);
	
	
	
//	System.out.println(Afastamento_temp.getData_iniAfast().getTime());
//	System.out.println(Afastamento_temp.getData_fimAfast().getTime().toString());
//	System.out.println(Afastamento_temp.getOnus());
//	System.out.println(Afastamento_temp.getMotivo_afast());
	
//	System.out.println(inicio_afastamento.toString());
//	System.out.println(inicio.toString());
//	System.out.println("inicio");
//	
//	System.out.println(fim_afastamento.toString());
//	System.out.println(fim.toString());
//	System.out.println("inicio");
	
//	inicio.setTime(dateFormat.par(inicio_afastamento.replaceAll("-","/")));
//	Afastamento_temp.setData_iniAfast(cal2);
//	fim.setTime(dateFormat.parse(fim_afastamento.replaceAll("-","/")));
//	Afastamento_temp.setData_fimAfast(cal3);
//
//	Afastamento_temp.setData_fimAfast(fim_afastamento);
//	Afastamento_temp.setData_iniAfast(inicio_afastamento);
//	Afastamento_temp.setOnus(onus);
	
	
	
	
	
	//Map<String,Object> map =  context.getAttributes();
	
	//novo_Afastamento.setNome_cidade();
	
	
	
	
	//System.out.println(calendar.toString());
	//System.out.println("\n entrou no metodo POST!!!!!!!");
	
	//System.out.println(nome_evento);
	context.getSession().save(context);
	return Results.redirect("/scap/cadastroEvento");
	

}
//Esse método é para teste apenas, no futuro, n havera como acessar o segundo formulario sem ir ao primeiro antes
public Result cadastroEvento(Context context){
	
	 return Results.ok(); 
}



public Result processaFormP2(
		@Param("tipoAfastamento") TipoAfastamento tipoAfastamento
		,@Param("nomeEvento") String nomeEvento
		//,@Param("matricula") String matricula
        ,@Param("cidadeEvento") String cidadeEvento
        ,@Param("inicioEvento") Date inicioEvento
        ,@Param("fimEvento") Date fimEvento
        ,Context context
        
        //,Afastamento novo_Afastamento
        //,Afastamento Afastamento_temp
		){
//	System.out.print("Estamos no segundo Form.pegou matricula da session: ");
//	System.out.println(context.getSession().get("matricula"));

	
	
//	System.out.print("Matricula do professor: ");
//	System.out.println(context.getSession().get("matricula"));
	
			
	
			inicio.setTime(inicioEvento);
			fim.setTime(fimEvento);
			dateFormat.setCalendar(inicio);
			dateFormat.setCalendar(fim);
			calendar=Calendar.getInstance();
			
			dateFormat.setCalendar(calendar);
			//LocalDate dataSolic;
			
			Afastamento_temp.setTipoAfastamento(tipoAfastamento);
			Afastamento_temp.setNome_evento(nomeEvento);
			Afastamento_temp.setNome_cidade(cidadeEvento);
			Afastamento_temp.setData_iniAfast(inicio);
			Afastamento_temp.setData_fimAfast(fim);
			
			
			
			Afastamento_temp.setData_iniEvento(inicio);
			Afastamento_temp.setData_fimEvento(fim);
		
			Afastamento_temp.setData_criacao(calendar);
			
			
							
			String matriculaParaBuscar= new String();
			System.out.print("buscando matricula do objeto da Singletown: ");
			matriculaParaBuscar=matriculaSolicitante;
			
			try{
				solicitante= aplPessoa.buscaMatricula(matriculaParaBuscar);
		    	}catch(RuntimeException e){
		    		System.out.println(e.getClass().getName());
		    	}
				
			
			System.out.println(matriculaParaBuscar);
//			if(matriculaParaBuscar.isEmpty()){
//				System.out.println("variavel local n leu da sessao");
//			}if(session.get("matricula").isEmpty()){
//				System.out.println("variavel da sessao nao foi lida");
//			}
			solicitante=aplPessoa.buscaMatricula(matriculaParaBuscar);
			
			
			
			
			inserir_afastamento.salvar(Afastamento_temp, solicitante, Afastamento_temp.getTipoAfastamento(),Afastamento_temp.getOnus());
			
			
			String resposta="Afastamento cadastrado  com Sucesso";
			
			 Map<String, Object> map = Maps.newHashMap();
		        map.put("resposta", resposta);
			
		      return Results.redirect("/scap/");	
		      //return Results.redirect("/scap/").render("resposta", resposta);
}
}



//public class CadastramentoController {
//	
//	//@Inject
//	
//public Result afastamento() {
//    	//Context context, Afastamento contact
//		Afastamento teste= new Afastamento();
//		
//		teste.setNome_evento("Jacque");
//		
////		 Map<String, Object> map = Maps.newHashMap();
////	        map.put("ObjJacque", teste);
//	        
//	        
//	        return Results.html().render("ObjJacque", teste);
//		
//		
//
//    }
//
//public Result contactForm(Context context, Afastamento contact) {
//	//
//	if(contact != null)
//	contact.setId_afastamento(contact.getId_afastamento()+1);
//	else {contact=new Afastamento();}
//	
////	 Map<String, Object> map = Maps.newHashMap();
////        map.put("ObjJacque", teste);
//        
//        
//        return Results.html().render(contact);
//	
//	
//
//}
//}
