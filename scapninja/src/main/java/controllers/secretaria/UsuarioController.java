package controllers.secretaria;


import ninja.Result;
import ninja.Results;
import ninja.params.Param;
import ninja.session.Session;
import ninja.Context;


import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;

import aplicacao.secretaria.AplPessoa;
import models.nucleo.Pessoa;

@Singleton
public class UsuarioController {
	
	@Inject
	AplPessoa aplPessoa;
	@Inject
	Pessoa pessoa;
	
	public Result inicializaCadastro(){
		
		return Results.html();
	}
	
	@Transactional
	public Result cadastraUsuario(Context contex
	,@Param("nome") String nome
	,@Param("sobreNome") String sobreNome
	,@Param("telefone") String telefone
	,@Param("email") String email
	,@Param("senha") String senha
	,@Param("matricula") String matricula
	,@Param("cargo") String cargo
	)
	{
//		try{
//			
//			aplPessoa.buscaMatricula(matricula);
//			
//		}catch{
//			
//		}
		pessoa.setEmail(email);
		pessoa.setMatricula(matricula);
		pessoa.setSobreNome(sobreNome);
		pessoa.setNome(nome);
		pessoa.setPassword(senha);
		pessoa.setTelefone(telefone);
			
		if(cargo.equals("PROFESSOR")){
			pessoa.setTipoPessoa("1");
		}else{
			pessoa.setTipoPessoa("2");
		} 
		aplPessoa.salvar(pessoa);
		return Results.redirect("/scap");
		}
}