/**
 * Copyright (C) 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.jpa.UnitOfWork;
import ninja.params.Param;
import ninja.session.Session;


import java.util.Map;

import javax.websocket.Endpoint;
import javax.websocket.MessageHandler;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.servlet.RequestScoped;
import com.google.inject.servlet.SessionScoped;

import models.nucleo.Pessoa;
import aplicacao.secretaria.AplPessoa;
import dao.UserDao;

@SessionScoped
public class LoginLogoutController /*extends Endpoint implements MessageHandler*/ {
    
    @Inject
    AplPessoa aplPessoa;
    
    @Inject 
    Pessoa usuario;
    ///////////////////////////////////////////////////////////////////////////
    // Login
    ///////////////////////////////////////////////////////////////////////////
    public Result login() {

        return Results.html();

    }
    
@UnitOfWork
@SessionScoped
    public Result loginPost(@Param("username") String username
                            ,@Param("password") String password
                            ,@Param("rememberMe") Boolean rememberMe
                            ,Session session
                            ,Context context
                            ) {
    	
		
		
		String nomeUsuario;
    	session.init(context);
    	String id = session.getId();
    	System.out.print("id do controlador do login: ");
    	System.out.println(id);
    	
    	try{
        usuario = aplPessoa.buscaMatricula(username);
    	}catch(RuntimeException e){
    		System.out.println(e.getClass().getName());
    	}
    	
        if (usuario.getPassword().equals(password)) {
            
            
            session.put("username",usuario.getMatricula());
            
            //session.put("sobrenome", usuario.getSobreNome());
            nomeUsuario= usuario.getNome();
            session.put("nome", nomeUsuario);
            session.save(context);
            
           
            System.out.println("nome do funcionario, print no logincontroller");
    		System.out.println(session.get("nome"));
    		
            
            if (rememberMe != null && rememberMe) {
                session.setExpiryTime(24 * 60 * 60 * 1000L);
            }
            
    	     //   return Results.html().render("ObjCinthia", teste);
            
            //codigo de exemplo
//            context.getFlashScope().success("login.loginSuccessful");
//            result.render("cargo",usuario.getTipoPessoa());
            
            
//            System.out.println(usuario.getNome());
//            System.out.println(usuario.getMatricula());
	        
            
//            Map<String, Object> map = Maps.newHashMap();
//            
//            String nome=usuario.getNome();
//            map.put("nome", nome);
//            result.render("nome",nome);
//            
//            String sobreNome=usuario.getSobreNome();
//            map.put("sobreNome", sobreNome);
//            result.render("sobreNome",sobreNome);
//            
//            String matricula=usuario.getMatricula();
//            map.put("matricula", matricula);
//            result.render("matricula",matricula);
            
//            Result result = Results.html();
//            result.render("nome",username).redirect("/scap");
//            return result;
            
            
            return Results.redirect("/scap");
            
//	        result.render("nome", usuario.getNome());
//            result.render("matricula", usuario.getMatricula());
//            result.render("sobreNome",usuario.getSobreNome());
//	        result= result.render("auth",usuario);
	       
            
            

        } else {
        	System.out.println("não passou do IF da senha");
            // something is wrong with the input or password not found.
            //context.getFlashScope().put("username", username);
           // context.getFlashScope().put("rememberMe", rememberMe);
            //context.getFlashScope().error("login.errorLogin");

            return Results.redirect("/scap/login");

        }

    }

    ///////////////////////////////////////////////////////////////////////////
    // Logout
    ///////////////////////////////////////////////////////////////////////////
    public Result logout(Context context) {

        // remove any user dependent information
        context.getSession().clear();
        //context.getFlashCookie().success("login.logoutSuccessful");

        return Results.redirect("/");

    }

}
