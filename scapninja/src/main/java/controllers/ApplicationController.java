/**
 * Copyright (C) 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.List;
import java.util.Map;


import ninja.Result;
import ninja.Results;
import ninja.session.Session;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.servlet.RequestScoped;
import com.google.inject.servlet.SessionScoped;

import ninja.Context;
import models.nucleo.Pessoa;

//import models.Article;
//import dao.ArticleDao;
//import dao.SetupDao;

@SessionScoped
public class ApplicationController {

//    @Inject
//    ArticleDao articleDao;
//
//    @Inject
//    SetupDao setupDao;
	
//	@Inject
//	Pessoa credenciais;
		
    public ApplicationController() {

    }

    /**
     * Method to put initial data in the db...
     * 
     * @return
     */
    public Result setup() {

//        setupDao.setup();
    	
    	
        return Results.ok();

    }

    public Result index(Context context, Session session) {
    	
    	//session.setExpiryTime(24 * 60 * 60 * 1000L);
    	
    	session.init(context);
    	String id = session.getId();
    	System.out.print("id do Controlador do Index: ");
    	System.out.println(id);
    	if(session.get("nome") != null){
    		
//    		pessoa.setMatricula(session.get("username"));
//    		pessoa.setNome(session.get("nome"));
//    		pessoa.setSobreNome(session.get("sobreNome"));
    		System.out.println("nome do funcionario , print da Index");
    		System.out.println(session.get("nome"));
    		System.out.println("print do tipo pessoa");
//    		System.out.println(pessoa.getNome());
//    		 Map<String, Object> map = Maps.newHashMap();
//    		 map.put("pessoa", pessoa);
//    		 System.out.println("Entrou no IF do index");
//    		return Results.html().render("pessoa",pessoa); 
    		return Results.html();
    	}
    	
    	System.out.println("Não entrou no IF do index");
    	return Results.html();
    		
    		
//        Article frontPost = articleDao.getFirstArticleForFrontPage();
//
//        List<Article> olderPosts = articleDao.getOlderArticlesForFrontPage();
//
//        Map<String, Object> map = Maps.newHashMap();
//        map.put("frontArticle", frontPost);
//        map.put("olderArticles", olderPosts);
//
//        return Results.html().render("frontArticle", frontPost)
//                .render("olderArticles", olderPosts);

    }
}
